// PROBLEME
// Tester la routine avec Lost
// effacer les routines moteur (avancer etc) et mettre des trucs inoffencifs à la place pour tester sur la table.
// Diminuer delay tourner à gauche ou droite quand il voit un visage
// ajouter une led connectée au BBB pour le mode visage
// Augmenter (doubler) le temps avant de perdre le visage
// ajouter des modes pour les nouvelles musiques
// c'est bien sinon :-)

//
//          Dictionnaire serial:
//          f = entrer mode detection de visage
//          a = avance
//          r = recule
//          g = gauche
//          d = droite
//          e = exploration
//          j = joyeux (buzzer)
//          t = triste (buzzer)
//          p = frustré (buzzer)
//          h = horreur - peur (buzzer)
//          f = detection visage (buzzer)
//          m = Changer de mode (vers detection visage ou vers explo)
//          ?? = Initialisation (rien ne se passe)
//          s  = melodie (buzzer)
//          x  = frustré 2 (buzzer rapide)
//          y  = modem (buzzer rapide comme une connection 56k)
//          z  = mystere (buzzer)
//          ?  = randomsong


// -- Buzzer --
//      * note 	frequency 	                period 	timeHigh
//       * c 	do        261 Hz 	        3830 	1915 	
//       * d 	re        294 Hz 	        3400 	1700 	
//       * e 	mi        329 Hz 	        3038 	1519 	
//       * f 	fa        349 Hz 	        2864 	1432 	
//       * g 	sol       392 Hz 	        2550 	1275 	
//       * a 	la        440 Hz 	        2272 	1136 	
//       * b 	si        493 Hz 	        2028	1014	
//       * C	do        523 Hz	        1912 	956
//       *




// The circuit:
// * RX is digital pin 0 (connect to TX of other device)
// * TX is digital pin 1 (connect to RX of other device)


char SerialInByte = 'x';

// Ajouter résistance de 500 Ohm (2-300 si il faut)
int speakerPin = 11; // La 10 est cassée
//-- MOTEUR A --
const int ENA=3; //Connecté à Arduino pin 3(sortie pwm)
const int IN1=4; //Connecté à Arduino pin 4
const int IN2=5; //Connecté à Arduino pin 5

//-- MOTEUR B --
const int ENB=9; //Connecté à Arduino pin 6(Sortie pwm)
const int IN3=7; //Connecté à Arduino pin 7
const int IN4=8; //Connecté à Arduino pin 8

int vitesse_casual = 170; // 0 pour les tests à table
int vitesse_serial = 130; // vitesse ralentie quand on fait autre chose
float ratio = 1.0;
int pourcentage = 10;

// -- LED -- 
const int ledRouge =  13; 
const int ledVerte =  12; 

// --- IR ---
boolean etatIR = 0; // state IR : voit un objet ou pas
long IRdistance = 0.0;
int IRPin = 0; // Pin analogique pour lecture de la tension de sortie du senseur (Vout).
// La tension varie entre 0 et 3.3 volts.


int bumper = 0; // Pare-choc Interrupt 0 est sur la pin 2
volatile boolean boom = 0; // accident avec le pare-choc ou pas
unsigned long previousMillis = 0;        // enregistre millis scanner IR
unsigned long previousMillisSTOP = 0;        // enregistre millis scanner IR
unsigned int intervalIR = 50;           // interval at which to blink (milliseconds)
unsigned int intervalSTOP = 1500000;           // interval at which to make music and dance (if no face detected

// ________________________________________________________________________________________________
// --- SETUP ---
void setup() {
  // set the digital pin as output:
  // pinMode(speakerPin, OUTPUT);
  pinMode(ENA,OUTPUT);//Configurer les broches comme sortie
  pinMode(ENB,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  digitalWrite(ENA,LOW);// Moteur A - Ne pas tourner (désactivation moteur)
  digitalWrite(ENB,LOW);// Moteur B - Ne pas tourner (désactivation moteur)
  pinMode(ledRouge, OUTPUT);   
  pinMode(ledVerte, OUTPUT);   

  pinMode(speakerPin, OUTPUT);
  pinMode(0,INPUT);
  pinMode(1,OUTPUT);

  Serial.begin(115200);
  // reserve 200 bytes for the inputString:
  Serial.println("Ready");
  Serial.println("Calibration and then explo");
  attachInterrupt(bumper, Choc, RISING); // autre possibilités FALLING ou LOW

 // if analog input pin 4 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(4));

  delay(1000);

  calibration();
  // 

}

void loop()
{

  int vitesse = vitesse_casual;
  explo(); // une boucle évènement à part: c'est le mode explorateur.
  // si explo se termine, ca veut dire qu'on joue avec les visages, on entre dans un autre mode
  // on peut commencer à jouer de la musique etc



  //  POUR TESTS SANS EXPLORER
  // attention serialevent est appelé à chaque boucle
  // donc si serialinbyte change on le verra (cf autres instructions provenant du BBB)
  //  if(SerialInByte == 'm'){
  detectfaces();
  //  }




}

// Explo loop

void explo()
{
  int vitesse = vitesse_casual;
  IRPin = 0;
  boolean r = false;
  int lost  = 1; // on commence au premier accident pour utilier un tableau de 3 valeurs dans t accident
  unsigned long delta = 0;
  unsigned long taccident[] = {
    0,0,0,0              };
  taccident[0] = millis();
  while(r == false){
    digitalWrite(ledVerte, HIGH);
    digitalWrite(ledRouge, LOW);

    if(boom == 1) {
      // Si les bumper sont activés, on a heurté quelque chose, boom = 1
      recule(vitesse);
      delay(600);
      gauche(vitesse);
      delay(180);
      boom = 0;
    }
    etatIR= 0 ; // on voit rien
    // commenter pour tests
    avance(vitesse); 
    unsigned long currentMillis = millis();
    // SCAN IR
    if(currentMillis - previousMillis > intervalIR) {  
      previousMillis = currentMillis;   
      etatIR = scan(etatIR);
      if(etatIR == 1){
        taccident[lost] = currentMillis;
        digitalWrite(ledVerte, LOW);
        digitalWrite(ledRouge, HIGH);
        recule(vitesse);
        delay(500);
        // si le capteur de droite a détecté, on va a gauche.
        // si le capteur de gauche a détecté, on va à droite
        if ( IRPin ==0){
          // position servo tourné a gauche et on va a droite.
          gauche(vitesse);
          //      Serial.println("GAUCHE");
          delay(400);   // 400 = demi-tour
        }
        else{
          droite(vitesse);
          //  Serial.println("DROITE");
          delay(400);
        }
        lost =+1; 
        // DEBUG
//        Serial.println("accident");
//        Serial.print("lost = ");
//        Serial.println(lost);
//        Serial.println(taccident[0]);
//        Serial.println(taccident[1]);
//        Serial.println(taccident[2]);
//        Serial.println(taccident[3]);
        delta = taccident[lost] - taccident[0];  // Delta = time between this accident and the beginning
//        Serial.print("delta = ");
//        Serial.println(delta);

        if(lost > 3){
          if (delta < 6000){
            // si on se cogne 3 fois en quelques secondes, on est dans les pieds de chaise, frustration !!
            frustre();
            // Number of milliseconds since the program started (unsigned long) 
            taccident[0] = millis();
            taccident[1] = 0;
            taccident[2] = 0;
            taccident[3] = 0;
            frustre2();
   //         recule(vitesse_serial);// se sortir de là
            delay(5000); // vient me chercher
            mystere();
            delay(3000);  
          }
          lost = 1;           // reinitialise compteur d accident
          delta = 0;
        }

      } 

      if(currentMillis - previousMillisSTOP > intervalSTOP){
        // Stop a little moment, turn  make a sound and go on
        previousMillisSTOP = currentMillis;   
        arret();
        delay(2000);  // on s arrête et on fait de la musique
//        modem();
//        modem();
        modem();
//        modem();
        gauche(255);  
        delay(800);  // tourne dans un sens
        mystere();
        droite(255);
        delay(900);  // tourne sur lui même dans l autre sens
//        melo();  // musique
        arret();
        delay(3000);
      } 
      IRPin = !IRPin; // Changement état de la pin pour basculer d'un 
    }

    // Serial event pas utilisé ici, il faut lire à la main
    // read valeur 
    serialEvent();
    if(SerialInByte == 'm'){
      r = true;  // on change de mode, on va détecter des visages
    }
  }
}


boolean detectfaces()
{
  // Si on est dans le mode detect face, arduino se fait piloter par le BBB

  int vitesse = vitesse_serial;
   Serial.println("detect");
  boolean r = false;
  while(r == false){
    if (Serial.available() > 0) {
      serialEvent();
      //    Serial.println("alors");
      switch (SerialInByte) {
      case 'a':
        avance(vitesse);
        delay(200);
        Serial.println("avance");
        break;
      case 'r':
        recule(vitesse);
        delay(200);
        Serial.println("recule");
        break;
      case 'g':
        gauche(vitesse);
        delay(100);
        Serial.println("gauche");
        break;        
      case 'd':
        droite(vitesse);
        delay(100);
        Serial.println("droite");
        break; 
        case '1':
        delay(1000);
        Serial.println("wait 1s");
        break;      
      case '2':
        delay(2000);
        Serial.println("wait 2s");
        break; 
       case '?':
        // random
        randomsong(6,500);   // 10 = middle, 5 = too short and 15 = song too long, boring
        break;     
      case 'w':
        // smoke on the water
        water();
        Serial.println("smoke on the water");
        break;         
      case 'h':
        // horror
        horror();  
        recule(vitesse);
        delay(400);
        gauche(vitesse);
        delay(300);
        avance(vitesse);
        delay(300);
        droite(vitesse);
        delay(300);
        Serial.println("horreur");
        frustre2();
        break;           
      case 't':
        // triste
        sad();
        break;        
      case 'f':
        // Déction de visage, je te reconnais
        visage();
        break; 
      case 'p':
        // frustration
        frustre();
        break;   
      case 'x':
        //autre forme de frustration (tempo rapide)
        frustre2();
//        delay(200);
//        modem();
        break;           
      case 's':
        // melody
        melo();
        break;     
      case 'y':
        // genre je BUG
        modem();
//        frustre2();
        break;  
      case 'z':
        mystere();
        break;       
      case '$':
        // melody
        gamme();
        break;      
      case 'e':
        r = true;  // Sortir de la bouche de détection et entrer dans le mode exploration
        break;              
      default: 
        break;
      }  
    }

  }

}
// __________________________






// --- IR ROUTINE --- 
boolean scan(int etatIR)
{
  // mesure la distance aux obstacles
  int valeur = analogRead(IRPin);  
  delay(275);
  valeur = analogRead(IRPin);  
  delay(40);
  valeur = analogRead(IRPin);  // la première valeur peut être foireuse si on a plusieurs senseurs. C'est une précaution
  // Converti la lecture en tension
  float tension = valeur*0.0048828125;
  float distance = 65*pow(tension, -1.10);
//    Serial.println(distance);  // 1 = gauche, 0 = droite
  //  Serial.print(" PIN "); 
  //  Serial.println(IRPin);
  if (distance < 75){  // ou 75 selon les cas. 75 marche pas si mal
    //       Serial.print("Obstacle : ");
    //       Serial.print(distance); 
    //       Serial.print(" PIN "); 
    //       Serial.println(IRPin);
    arret();
    etatIR = 1 ;
  }
  return etatIR; 
}










