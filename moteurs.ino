// --- MOTOR ROUTINE --- 
void arret(){
  digitalWrite(ENA,LOW);// Moteur A - Ne pas tourner (désactivation moteur)
  digitalWrite(ENB,LOW);// Moteur B - Ne pas tourner (désactivation moteur)

  digitalWrite(IN1,LOW); //On coupe le courant
  digitalWrite(IN2,LOW);

  digitalWrite(IN3,LOW); // On coupe le courant
  digitalWrite(IN4,LOW);


}
// avance
void avance(int vitesse){

//  // Direction du Moteur A
  digitalWrite(IN1,HIGH); // On avance
  digitalWrite(IN2,LOW);

  digitalWrite(IN3,HIGH); // On avance
  digitalWrite(IN4,LOW);
  int vitesseR;  // right wheel faster than the left
  vitesseR = vitesse*ratio;  // Ratio = 1.0 at the beginning but < 1 when calibration is well performed
  analogWrite(ENA,vitesse);
  analogWrite(ENB,vitesse);


}
// recule
void recule(int vitesse){
//  // Direction du Moteur A
  digitalWrite(IN1,LOW); // On recule
  digitalWrite(IN2,HIGH);

  digitalWrite(IN3,LOW); // On recule
  digitalWrite(IN4,HIGH);

  analogWrite(ENA,vitesse);
  analogWrite(ENB,vitesse);
}

void droite(int vitesse){
//  // Direction du Moteur A
  digitalWrite(IN1,HIGH); // On avance
  digitalWrite(IN2,LOW);

  digitalWrite(IN3,LOW); // On recule
  digitalWrite(IN4,HIGH);

  analogWrite(ENA,vitesse);
  analogWrite(ENB,vitesse);
}

void gauche(int vitesse){
//  // Direction du Moteur A
  digitalWrite(IN1,LOW); // On recule
  digitalWrite(IN2,HIGH);

  digitalWrite(IN3,HIGH); // On avance
  digitalWrite(IN4,LOW);

  analogWrite(ENA,vitesse);
  analogWrite(ENB,vitesse);
}

void down(){
  // Tout couper pour la musique
  digitalWrite(ENA,LOW);// Moteur A - Ne pas tourner (désactivation moteur)
  digitalWrite(ENB,LOW);// Moteur B - Ne pas tourner (désactivation moteur)

  digitalWrite(IN1,LOW); //On coupe le courant
  digitalWrite(IN2,LOW);

  digitalWrite(IN3,LOW); // On coupe le courant
  digitalWrite(IN4,LOW);

  digitalWrite(ledVerte, LOW);
  digitalWrite(ledRouge, LOW);
  digitalWrite(speakerPin, LOW);

}


