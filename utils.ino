// ________________________________________________________________________________________________
// --- ROUTINES ---

void serialEvent() {
  /*  SerialEvent occurs whenever a new data comes in the
   hardware serial RX.  This routine is run between each
   time loop() runs, so using delay inside loop can delay
   response.  Multiple bytes of data may be available.
   */
  char msg;
  while(Serial.available()) {  // lire tant qu'il y a des données
    //  Get the number of bytes (characters) available for reading from the serial port. This is data that's already arrived and stored in the serial receive buffer 
    //  (which holds 64 bytes). available() inherits from the Stream utility class. 
    // get the new byte:
    SerialInByte = Serial.read(); // read all the bites. Only the oldest byte in the serial buffer will remains: 
    if ( SerialInByte != 13){     // if the last character is not a Carriage return, code 13
      msg = SerialInByte;
    }
  }
  SerialInByte = msg; // just got the command and not the terminating char

}

// --- Interruption --- 
void Choc()
{
  int vitesse = vitesse_casual;
  digitalWrite(ledVerte, LOW);
  digitalWrite(ledRouge, HIGH);
  boom = 1;
  arret();
  recule(vitesse);

}

void calibration()
{
  int vitesse = vitesse_casual;
  digitalWrite(ledVerte, LOW);
  digitalWrite(ledRouge, HIGH);
  calibration_start();
  Serial.println("cc"); // tells BBB that you want to perform calibration
  // Calibration= avancer (pas trop vite) et prendre plein de photos (j'espère que ce sera fluide), enregistrer chaque frame. comparer les images deux à deux et voir vers où on dévie
  // recommencer à avancer, reculer plusieurs fois jusqu'à ce que la déviation soit suffisament faible
  int i = 0;
  int number2 = 2;
  float quality; // quality factor; when number tend to a contant, it's OK, the robot goes in straight line
//  serialEvent();
//  if(SerialInByte == 'c'){
//    while (quality > 1.5 || quality < 0.66){
//      avance(vitesse);
//      delay(200);
//      serialEvent();
//      int number = SerialInByte - '0';  // test conversion char to int for vitesse // pourcentage force sur roue droite (deviation à gauche, roue droite trop rapide)
//      if (number < 10 && number > 0){
//        ratio = number / 10.0;
//        quality = number/number2;
//        number2 = number;
//      }
//      else{
//        number = 1;
//      }
//      recule(vitesse);
//      delay(200);
//    }  
//
//
//  }
//  
  
  ratio =10 / 10.0;
   Serial.println("Calibration terminated");
  calibration_stop();

}



// ________________________

// MUSIQUE

void calibration_start(){
  down();
  //   "dergmfhsjlkbD "
  int length = 7; // the number of notes
  char notes[] = {
    "dddmmmd"        }; // a space represents a rest
  //    char notes[] = {" llll ffff"}; // a space represents a rest
  int beats[] = { 
    1,1,1,1,1,1,2       };
  int tempo = 200;
  song(length, notes, beats, tempo);
}


void calibration_stop(){
  down();
  //   "dergmfhsjlkbD "
  int length = 3; // the number of notes
  char notes[] = {
    "ddd"        }; // a space represents a rest
  //    char notes[] = {" llll ffff"}; // a space represents a rest
  int beats[] = { 
    1,1,2        };
  int tempo = 100;
  song(length, notes, beats, tempo);
}



void frustre(){
  down();
  //   "dergmfhsjlkbD "
  int length = 10; // the number of notes
  char notes[] = {
    " lllr fffe"        }; // a space represents a rest
  //    char notes[] = {" llll ffff"}; // a space represents a rest
  int beats[] = { 
    1,1,1,1,4,1,1,1,1,6        };
  int tempo = 100;
  song(length, notes, beats, tempo);
}

void water(){  // somke on the water
  down();
  int length = 12; // the number of notes
  char notes[] = { 
    "rfsrfksrfsfr"         }; // a space represents a rest
  int beats[] = {  
    1,1,3,1,1,1,4,1,1,3,1,4          };
  int tempo = 300;
  song(length, notes, beats, tempo);

}

void sad(){
  down();
  int length = 6; // the number of notes
  char notes[] = {
    "fhfmgr"            }; // a space represents a rest
  int beats[] = {
    1,2,1,1,1,2            };
  int tempo = 250;
  song(length, notes, beats, tempo);
}

void gamme(){
  down();
  int length = 13; // the number of notes
  char notes[] = { 
    "dergmfhsjlkbX "            }; // a space represents a rest
  int beats[] = { 
    1,1,1,1,1,1,1,1,1,1,1,1,1            };
  int tempo = 450;
  song(length, notes, beats, tempo);

}

void visage(){
  down();
  int length = 7; // the number of notes
  char notes[] = {
    "sfmdfsf"        }; // a space represents a rest
  int beats[7] = { 
    1,1,1,1,1,1,3            };
  int tempo = 200;
  song(length, notes, beats, tempo);

}

void horror(){
  down();
  int length = 7; // the number of notes
  char notes[] = {
    "mgd"        }; // a space represents a rest
  int beats[3] = { 
    1,1,4     };
  int tempo = 100;
  song(length, notes, beats, tempo);

}



void melo(){
//  mi re mi re do sol fa re do
  down();
  int length = 9; // the number of notes
  char notes[] = {
    "mrmrdsfrd"        }; // a space represents a rest
  int beats[9] = { 
    1,1,1,1,1,3,2,2,3     };
  int tempo = 200;
  song(length, notes, beats, tempo);

}


void frustre2(){
//  fa fa fa fa fa re sol mi do
  down();
  int length = 9; // the number of notes
  char notes[] = {
    "fffffrsmd"        }; // a space represents a rest
  int beats[9] = { 
    1,1,1,1,1,6,3,3,6     };
  int tempo = 50;
  song(length, notes, beats, tempo);

}

void modem(){
//  sssrdlll
  down();
  int length = 8; // the number of notes
  char notes[] = {
    "sssrdlll"        }; // a space represents a rest
  int beats[8] = { 
    1,1,1,2,4,1,1,6     };
  int tempo = 50;
  song(length, notes, beats, tempo);

}

void mystere(){
//  fa la b fa mi re
  down();
  int length = 9; // the number of notes
  char notes[] = {
    "fkfmr"        }; // a space represents a rest
  int beats[9] = { 
    2,1,1,2,4     };
  int tempo = 150;
  song(length, notes, beats, tempo);

}

void randomsong(int lengthrange, int trange){
  down();
  char p[] = { 
    "dergmfhs "            }; // a space represents a rest
 int lenght  = random(2, lengthrange);
 int beat;
 char note;
 for (int i = 0;i < lenght; i++){
   // sizeof(p) = 9 pour les 8 lettres, l espace et le '\0'
   note = p[rand()%sizeof(p)]; // 14 notes --> 13 possibilities
   beat = random(25,trange);  // lenght of note in ms
   if (note == ' '){
   delay(beat);
   }
   else {
     playNote(note, beat);
   }
 }
   

}


//_____________________________________________________________________
void song(int length, char notes[], int beats[], int tempo)
{
  for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } 
    else {
      playNote(notes[i], beats[i] * tempo);
    }   
    // pause between notes
    delay(tempo / 2); 
  } 

}

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);
  }
}

//      * note 	frequency 	                period 	timeHigh
//       * d 	do        261 Hz 	        3830 	1915 	
//       * e    do dieze                        3615  1807.5
//       * r 	re        294 Hz 	        3400 	1700 	
//       * g     mi bemol                       3219  1609.5
//       * m 	mi        329 Hz 	        3038 	1519 	
//       * f 	fa        349 Hz 	        2864 	1432 	
//       * h    fa dieze                        2707  1353.5
//       * s	sol       392 Hz 	        2550 	1275 	
//       * j     sol dieze                      2411  1205.5
//       * l 	la        440 Hz 	        2272 	1136 	// Ca coupe apd de là !!!
//       * k 	si bemol         	        2150  1075   
//       * b 	si        493 Hz 	        2028	1014	
//       * X	do        523 Hz	        1912 	956


void playNote(char note, int duration) {
  char names[] = { 
    'd', 'e', 'r', 'g', 'm', 'f', 'h', 's','j','l','k','b','X'             }; 
  int tones[] = {  
    1915,1807, 1700, 1609,1519, 1432,1353, 1275, 1205,1136, 1075,1014, 956             };

  // play the tone corresponding to the note name
  for (int i = 0; i < 8; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}



